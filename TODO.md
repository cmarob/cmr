# Static Case:
## Agent

1. Add self.state $\in \{$observation, dissemination$\}$, handle the switch between the 2 states(maybe a countdown of steps);
2. Add self.belief, self.estimate;
3. Avoid the resetting of self.step\_on\_target($n_1$) in control, add self.total_steps($n_0$);
4. Create broadcast function

## Arena

1. Handle broadcast messages ( can be handled by different class: Communicator )

# Dynamic Case:
## Arena

1. Handle agents eating targets

# Quorum Sensing:
## ...


# Questions for Trianni

1. Value added by Violetta, is that relevant;
